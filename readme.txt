
//Spring boot run
mvn spring-boot:run
//Spring boot run with debug port
mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"

//swagger page
http://localhost:8080/template/swagger-ui.html