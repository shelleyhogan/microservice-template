package com.example.microservice.repository;


import com.example.microservice.domain.AccountInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountInfoRepository extends JpaRepository<AccountInfo, String> {


}
