package com.example.microservice.controller;

import com.example.microservice.exception.NotFoundException;
import com.example.microservice.domain.AccountInfo;
import com.example.microservice.domain.response.AccessToken;
import com.example.microservice.service.ApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api")
public class ApiController {

    @Autowired
    private ApiService apiService;


    @GetMapping ("/token")
    public ResponseEntity<AccessToken> getToken() throws Exception {

        AccessToken token = apiService.getToken();
        if (token == null) {
            throw new NotFoundException("Only sample, No real token!!");
        }
        return new ResponseEntity<AccessToken>(token, HttpStatus.OK);
    }

    @GetMapping ("/account")
    public ResponseEntity<List<AccountInfo>> getAccountInfo() throws Exception {
        List<AccountInfo> accounts = apiService.getAccounts();
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

}
