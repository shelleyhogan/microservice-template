package com.example.microservice.service;


import com.example.microservice.domain.AccountInfo;
import com.example.microservice.domain.response.AccessToken;
import com.example.microservice.repository.AccountInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@Service
public class ApiService  {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private AccountInfoRepository accountInfoRepository;


    @Value("${domain_url}")
    private String domainUrl;


    public AccessToken getToken() {
        //AccessToken accessToken = authenticationService.getAccessToken();
        AccessToken token = new AccessToken();
        token.setAccessToken("Test Token");
        token.setTokenType("Type is test");
        return token;
    }

    public List<AccountInfo> getAccounts() {
        return accountInfoRepository.findAll();
    }


}
