package com.example.microservice.service;

import com.example.microservice.domain.AccountAccessToken;
import com.example.microservice.domain.requst.AccessTokenRequest;
import com.example.microservice.domain.response.AccessToken;
import com.example.microservice.util.ClientApiBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@Service
public class AuthenticationService {
    @Autowired
    private RestTemplate restTemplate;
    @Value("${oauth_url}")
    private String oauthUrl;
    @Value("${api_key}")
    private String username;
    @Value("${api_secret}")
    private String password;
    @Value("${grant_type}")
    private String grantType;
    private HashMap<String, AccessToken> tokenMap;




    public AccessToken getAccessToken() {
        AccessTokenRequest tokenRequest = new AccessTokenRequest();
        tokenRequest.setApiKey(username);
        tokenRequest.setApiSecret(password);
        tokenRequest.setGrantType(grantType);
        //get token from cache
        AccessToken accessToken = findAccessToken(tokenRequest.getApiKey());
        if(null==accessToken || !accessToken.isValidToken()) {
            final String url = oauthUrl;
            HttpHeaders headers = ClientApiBuilder.buildAccessTokenRequestHeaders(tokenRequest.getApiKey(), tokenRequest.getApiSecret());
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("grant_type", tokenRequest.getGrantType());
            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);
            accessToken = restTemplate.postForObject(url, entity, AccessToken.class);
            AccountAccessToken accountAccessToken = convertToAccountAccessToken(tokenRequest, accessToken);
            accessToken = accountAccessToken.getAccessToken();
            buildTokenCache(accountAccessToken);
        }
        return accessToken;
    }

    public HashMap<String, AccessToken> buildTokenCache(AccountAccessToken token) {
        if(tokenMap == null) {
            tokenMap = new HashMap<>();
        }
        tokenMap.put(token.getApiKey(), token.getAccessToken());
        return tokenMap;
    }

    private AccessToken findAccessToken (String apiKey) {
        if(null==tokenMap) {
            return null;
        }
        return tokenMap.get(apiKey);
    }

    private AccountAccessToken convertToAccountAccessToken(AccessTokenRequest tokenRequest, AccessToken token) {
        AccessToken accessToken = new AccessToken();
        accessToken.setAccessToken(token.getAccessToken());
        accessToken.setExpiresIn(token.getExpiresIn());
        return new AccountAccessToken(tokenRequest.getApiKey(), accessToken);
    }

}
