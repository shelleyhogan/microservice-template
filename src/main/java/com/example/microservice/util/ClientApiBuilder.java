package com.example.microservice.util;


import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class ClientApiBuilder {

    public static HttpHeaders buildHttpHeaders(String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + accessToken);
        return headers;
    }

    public static HttpHeaders buildAccessTokenRequestHeaders(String username, String password){
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(
                auth.getBytes(StandardCharsets.US_ASCII) );
            setBasicAuth(new String(encodedAuth));
            setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        }};
    }



    public static boolean isNotEmpty (String value) {
        return null != value && !value.trim().isEmpty();
    }

}
