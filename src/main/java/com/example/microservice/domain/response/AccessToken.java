package com.example.microservice.domain.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccessToken implements Serializable {

    private String accessToken;
    private String scope;
    private String tokenType;
    private long expiresIn;
    private long timeExpires;

    @JsonIgnore
    private final long createdTimeMills = System.currentTimeMillis();

    public boolean isValidToken() {
        timeExpires = getTimeExpires();
        if(0 !=timeExpires) {
            return timeExpires < System.currentTimeMillis() - 300;
        } else {

        }
        return false;
    }

    public long getTimeExpires() {
        timeExpires =  System.currentTimeMillis() + expiresIn;
        return timeExpires;
    }

}
