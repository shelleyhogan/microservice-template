package com.example.microservice.domain.requst;

import lombok.AllArgsConstructor;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccessTokenRequest implements Serializable {

    private String oauthUrl;
    private String apiKey;
    private String apiSecret;
    private String grantType;
}
