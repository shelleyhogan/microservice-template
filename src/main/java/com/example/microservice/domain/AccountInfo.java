package com.example.microservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;


@Entity
@Table (name = "account_info")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountInfo {

    @Id
    private String id;
    @Column (name = "practice_id")
    private String practiceId;
    @Column (name = "api_key")
    private String apiKey;
    @Column(name = "api_secret")
    private String apiScret;

    @PreUpdate
    @PrePersist
    protected void generateId() {
        if (null == id) {
            id = UUID.randomUUID().toString();
        }
    }
}
