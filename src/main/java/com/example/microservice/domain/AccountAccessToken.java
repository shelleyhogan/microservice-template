package com.example.microservice.domain;


import com.example.microservice.domain.response.AccessToken;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class AccountAccessToken {
    private String apiKey;
    private AccessToken accessToken;
}
