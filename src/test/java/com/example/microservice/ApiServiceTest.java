package com.example.microservice;


import com.example.microservice.domain.requst.AccessTokenRequest;
import com.example.microservice.domain.response.AccessToken;
import com.example.microservice.service.ApiService;
import com.example.microservice.service.AuthenticationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class ApiServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private final ApiService apiService = new ApiService();
    @Mock
    private AuthenticationService authenticationService;

    private AccessToken accessToken;

    @MockBean
    private AccessTokenRequest tokenRequest;

    @Before
    public void setup() {
        accessToken = new AccessToken();
        accessToken.setAccessToken("access_token");
        accessToken.setExpiresIn(3600);

    }

    @Test
    public void gethappyPathTest() {

        //sample code took from other project
        when(authenticationService.getAccessToken()).thenReturn(accessToken);

        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("practiceId", "P001");
        requestMap.put("patientId", "E001");
        HttpEntity entity = new HttpEntity(null);

        ResponseEntity response = mock(ResponseEntity.class);
        //when(response.getBody()).thenReturn(patient);
        //when(restTemplate.exchange("http://localhost:8080/P001/E001", HttpMethod.GET, entity, AthenaPatientResponse.class, requestMap))
        //   .thenReturn(new ResponseEntity(new AthenaPatientResponse(), HttpStatus.OK));
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), (Class<Object>) any(), anyMap())).thenReturn(response);
        

    }

}
